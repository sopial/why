# reverse-proxy-ops

A simple reverse proxy to service front and backend services.

# Migration to traefik doc
## Why did we change our reverse proxy from nginx to traefik ?

### Our infrastructure

We are in a dockerized environment, running docker services in a swarm cluster of 3 machines. 

We have one reverse proxy in front of our services, which is listening on IBM VPN on port 80, behind a IBM load balancer.

Because of our CICD processes, we are shuting down/re-up regularly every swarm services.

### Why a static DNS resolver isn't suitable for our needs ? 

Nginx free version use a statice DNS resolver. That means it resolves DNS only once, when you start it. Then, nginx keep the corresponding IP in memory and will never try to resolve it again. 

This is a problem:
When you remove a container/a service or a docker stack, then you re-create it with the same internal Alias on the cluster's swarm network, but docker is reallocating a new IP.

This is a problem for nginx, nginx has already resolved the DNS on the init, and nginx will keep the initial IP in memory as the DNS corresponding IP, so the service become unrecheable for nginx.

We have no other way to update the IP reminded by nginx than to restart nginx, it's not a good solution in a HA environment.

### Solutions

Nginx+ can implement a dynamic DNS resolver but it's not free.

We have chosen Traefik, it's a reverse proxy using a dynamic DNS resolver.
Also, Traefik is designed to work in a containerized environment. Traefik is binded with the docker socket and listening on docker events to detect if there is a change in the infrastructure to immediately adapt itself / it's own configurations. Traefik speak about dynamic configuration.

Also, Traefik provide a very clear dashboard, and a much more easier way to create configuration than Nginx does.

## Illustration of the static DNS pain

- We are in a swarm cluster
- We are only deploying swarm services 
- Every services are inside the same swarm network
- Every services are aliased to be contacted using this alias



Backend stack:
Docker-compose
```yaml
version: '3.4'

networks:
  webapp:
    external: true

services:
  backend_svc:
  # A service listeining on port 3000 and responding Hello World! on '/' route.
    image: webserver:1.0.0
    environment:
      - LISTENING_PORT=3000
    deploy:
      mode: global
    networks:
      webapp:
        aliases:
          - backend_app
```

Reverse proxy stack:
```yaml
version: '3.4'

networks:
  webapp:
    external: true

services:
  reverse_proxy_svc:
    image: nginx:1.14
    deploy:
      mode: global
    configs:
      - source: revProxyConf
        target: /etc/nginx/conf.d/default.conf
    ports:
      - 80:80
    networks:
      webapp:
        aliases:
          - reverse_proxy
```

The nginx config
```
server {
    listen                      80;
    server_name                 dev-tracabilite.projectX.com;

    location / {
        proxy_pass 	            http://backend_app:3000/;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;
        proxy_set_header        Host $http_host;
    }
}
```

We deploy it: 
```bash
$ docker stack deploy -c docker-compose-back.yml backend_stack
$ docker stack deploy -c docker-compose-revProxy.yml revProxy_stack
```

On the remonte machine, get the IP attribued to the back service : 

```bash
$ docker inspect --format="{{json .Endpoint.VirtualIPs}}" backend_svc | jq '.'
[
  {
    "NetworkID": "ucfwsb89177uni5puoke9k0xx",
    "Addr": "10.0.1.7/24"
  }
]
```

So if you step inside nginx service, so inside the docker network 'webapp' it's possible to do two same things: 
```bash
container_reverse_proxy>$ curl 10.0.1.7:3000/
Hello World!

container_reverse_proxy>$ curl http://backend_app:3000/
Hello World!
```

Outside, it's possible to request the service using the DNS:

```
$ curl dev-tracabilite.projectX.com
Hello World!
```

Now if you update the service, the docker internal IP should not change, everything is fine: 

```bash
$ docker update --force backend_svc
...
$ docker inspect --format="{{json .Endpoint.VirtualIPs}}" backend_svc | jq '.'
[
  {
    "NetworkID": "ucfwsb89177uni5puoke9k0xx",
    "Addr": "10.0.1.7/24"
  }
]
```

But if you remove and re-up the stack (or even only the service), then the IP changes

```bash
$ docker stack rm backend_stack 
# or just the service: $ docker service rm backend_svc
$ docker stack deploy -c docker-compose.yml backend_stack
$ docker inspect --format="{{json .Endpoint.VirtualIPs}}" backend_svc | jq '.'
[
  {
    "NetworkID": "ucfwsb89177uni5puoke9k0xx",
    "Addr": "10.0.1.27/24"
  }
]
```

So if you step inside nginx service, so inside the docker network 'webapp' it's possible to do three things: 
```bash
container_reverse_proxy>$ curl 10.0.1.7:3000/
curl: (7) Failed to connect to 10.0.1.7 port 3000: No route to host
# Doesn't work because swarm changend the internal ip adress of the service

container_reverse_proxy>$ curl http://back:3000/
Hello World!
# Works because you are using the docker DNS, wich is up-to-date with it's own services

container_reverse_proxy>$ curl http://10.0.1.27:3000/
Hello World!
# Works because you are using the new allocated internal IP
```

But if you are outside the docker network and you try to use the Nginx DNS resolver: 

```bash
$ curl dev-tracabilite.projectX.com
502 Bad gateway
```

This is because Nginx is still looking for the docker internal IP  `10.0.1.7`, and nothing is listening on it.